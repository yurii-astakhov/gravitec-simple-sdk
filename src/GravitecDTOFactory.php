<?php

declare(strict_types=1);

namespace GravitecSDK;

use GravitecSDK\DTO\AudienceDTO;
use GravitecSDK\DTO\PayloadDTO;
use GravitecSDK\DTO\PushDTO;

/**
 * Class GravitecDTOFactory.
 */
final class GravitecDTOFactory
{
    /**
     * @param string $title
     * @param string $message
     * @param string $redirectUrl
     * @param string $icon
     * @param array $tags
     * @param string|null $image
     * @param bool $isTransactional
     *
     * @return PushDTO
     */
    public static function createSimplePushDTO(
        string $title,
        string $message,
        string $redirectUrl,
        string $icon,
        array $tags = [],
        string $image = null,
        bool $isTransactional = true
    ): PushDTO {
        $payload = new PayloadDTO();
        $payload->setTitle($title);
        $payload->setMessage($message);
        $payload->setRedirectUrl($redirectUrl);
        $payload->setIcon($icon);

        if ($image) {
            $payload->setImage($image);
        }

        $audience = new AudienceDTO();
        $audience->setTags($tags);

        $dto = new PushDTO();
        $dto->setPayload($payload);
        $dto->setAudience($audience);
        $dto->setIgnoreDuplicate(false);
        $dto->setIsTransactional($isTransactional);

        return $dto;
    }
}
