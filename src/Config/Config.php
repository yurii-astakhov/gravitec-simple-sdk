<?php

declare(strict_types=1);

namespace GravitecSDK\Config;

/**
 * Class Config.
 */
class Config
{
    /**
     * @var string
     */
    private $appId;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $this->appId = (string)getenv('GRAVITEC_APP_ID');
        $this->apiKey = (string)getenv('GRAVITEC_API_KEY');
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getAuthorizationKey(): string
    {
        return 'Basic ' . base64_encode($this->getAppId() . ':' . $this->getApiKey());
    }
}
