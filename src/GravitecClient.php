<?php

declare(strict_types=1);

namespace GravitecSDK;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GravitecSDK\DTO\ButtonDTO;
use GravitecSDK\DTO\PushDTO;
use GravitecSDK\DTO\TagSegmentDTO;

/**
 * Class GravitecClient.
 */
class GravitecClient
{
    private const BASE__URI = 'https://uapi.gravitec.net/api/v3/';

    /**
     * @var Client
     */
    private $apiClient;

    /**
     * @var Config\Config
     */
    private $config;

    /**
     * GravitecClient constructor.
     */
    public function __construct()
    {
        $this->apiClient = new Client(['base_uri' => self::BASE__URI]);
        $this->config = new Config\Config();
    }

    /**
     * @param PushDTO $pushDTO
     *
     * @throws GuzzleException
     */
    public function sendPush(PushDTO $pushDTO): void
    {
        $this->apiClient->request(
            'POST',
            'https://uapi.gravitec.net/api/v3/push',
            $this->prepareData($pushDTO)
        );
    }

    /**
     * @param PushDTO $pushDTO
     *
     * @return array
     */
    private function prepareData(PushDTO $pushDTO): array
    {
        $data = [
            'send_date'        => $pushDTO->getSendDate(),
            'ttl'              => $pushDTO->getTtl(),
            'is_transactional' => $pushDTO->isTransactional(),
            'ignore_duplicate' => $pushDTO->isIgnoreDuplicate(),
            'segments'         => $pushDTO->getSegments(),
            'payload'          => [
                'title'        => $pushDTO->getPayload()->getTitle(),
                'message'      => $pushDTO->getPayload()->getMessage(),
                'icon'         => $pushDTO->getPayload()->getIcon(),
                'redirect_url' => $pushDTO->getPayload()->getRedirectUrl(),
            ],
            'audience'         => [
                'tokens'  => $pushDTO->getAudience()->getTokens(),
                'tags'    => $pushDTO->getAudience()->getTags(),
                'aliases' => $pushDTO->getAudience()->getAliases(),
            ]
        ];

        if ($pushDTO->getPushTag()) {
            $data['push_tag'] = $pushDTO->getPushTag();
        }

        if ($pushDTO->getPayload()->getImage()) {
            $data['payload']['image'] = $pushDTO->getPayload()->getImage();
        }

        if (!empty($pushDTO->getPayload()->getButtons())) {
            $data['payload']['buttons'] = array_map(static function (ButtonDTO $buttonDTO) {
                return [
                    'title' => $buttonDTO->getTitle(),
                    'url'   => $buttonDTO->getUrl(),
                ];
            }, $pushDTO->getPayload()->getButtons());
        }

        if (!empty($pushDTO->getAudience()->getTagsSegments())) {
            $data['audience']['tags_segment'] = array_map(static function (TagSegmentDTO $tagSegmentDTO) {
                return [
                    'association' => $tagSegmentDTO->getAssociation(),
                    'name'        => $tagSegmentDTO->getName(),
                ];
            }, $pushDTO->getAudience()->getTagsSegments());
        }

        return [
            'headers' => [
                'Authorization' => $this->config->getAuthorizationKey(),
                'Content-Type'  => 'application/json'
            ],
            'body'    => json_encode($data),
        ];
    }
}
