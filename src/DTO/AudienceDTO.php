<?php

declare(strict_types=1);

namespace GravitecSDK\DTO;

/**
 * Class AudienceDTO.
 */
final class AudienceDTO
{
    /**
     * @var string[]
     */
    private $tokens = [];

    /**
     * @var string[]
     */
    private $aliases = [];

    /**
     * @var string[]
     */
    private $tags = [];

    /**
     * @var TagSegmentDTO[]
     */
    private $tagsSegments = [];

    /**
     * @return string[]
     */
    public function getTokens(): array
    {
        return $this->tokens;
    }

    /**
     * @param string[] $tokens
     */
    public function setTokens(array $tokens): void
    {
        $this->tokens = $tokens;
    }

    /**
     * @return string[]
     */
    public function getAliases(): array
    {
        return $this->aliases;
    }

    /**
     * @param string[] $aliases
     */
    public function setAliases(array $aliases): void
    {
        $this->aliases = $aliases;
    }

    /**
     * @return string[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param string[] $tags
     */
    public function setTags(array $tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @return TagSegmentDTO[]
     */
    public function getTagsSegments(): array
    {
        return $this->tagsSegments;
    }

    /**
     * @param TagSegmentDTO[] $tagsSegments
     */
    public function setTagsSegments(array $tagsSegments): void
    {
        $this->tagsSegments = $tagsSegments;
    }
}
