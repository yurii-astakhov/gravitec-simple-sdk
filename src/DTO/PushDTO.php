<?php

declare(strict_types=1);

namespace GravitecSDK\DTO;

/**
 * Class PushDTO.
 */
final class PushDTO
{
    /**
     * @var string
     */
    private $sendDate = '';

    /**
     * @var int
     */
    private $ttl = 28800;

    /**
     * @var string|null
     */
    private $pushTag;

    /**
     * @var string
     */
    private $displayTime = '';

    /**
     * @var bool
     */
    private $isTransactional = false;

    /**
     * @var bool
     */
    private $ignoreDuplicate = true;

    /**
     * @var string[]
     */
    private $segments = [];

    /**
     * @var PayloadDTO
     */
    private $payload;

    /**
     * @var AudienceDTO
     */
    private $audience;

    /**
     * @return string
     */
    public function getSendDate(): string
    {
        return $this->sendDate;
    }

    /**
     * @param string $sendDate
     */
    public function setSendDate(string $sendDate): void
    {
        $this->sendDate = $sendDate;
    }

    /**
     * @return int
     */
    public function getTtl(): int
    {
        return $this->ttl;
    }

    /**
     * @param int $ttl
     */
    public function setTtl(int $ttl): void
    {
        $this->ttl = $ttl;
    }

    /**
     * @return string|null
     */
    public function getPushTag(): ?string
    {
        return $this->pushTag;
    }

    /**
     * @param string $pushTag
     *
     * @return void
     */
    public function setPushTag(?string $pushTag): void
    {
        $this->pushTag = $pushTag;
    }

    /**
     * @return string
     */
    public function getDisplayTime(): string
    {
        return $this->displayTime;
    }

    /**
     * @param string $displayTime
     *
     * @return void
     */
    public function setDisplayTime(string $displayTime): void
    {
        $this->displayTime = $displayTime;
    }

    /**
     * @return bool
     */
    public function isTransactional(): bool
    {
        return $this->isTransactional;
    }

    /**
     * @param bool $isTransactional
     *
     * @return void
     */
    public function setIsTransactional(bool $isTransactional): void
    {
        $this->isTransactional = $isTransactional;
    }

    /**
     * @return bool
     */
    public function isIgnoreDuplicate(): bool
    {
        return $this->ignoreDuplicate;
    }

    /**
     * @param bool $ignoreDuplicate
     */
    public function setIgnoreDuplicate(bool $ignoreDuplicate): void
    {
        $this->ignoreDuplicate = $ignoreDuplicate;
    }

    /**
     * @return string[]
     */
    public function getSegments(): array
    {
        return $this->segments;
    }

    /**
     * @param string[] $segments
     */
    public function setSegments(array $segments): void
    {
        $this->segments = $segments;
    }

    /**
     * @return PayloadDTO
     */
    public function getPayload(): PayloadDTO
    {
        return $this->payload;
    }

    /**
     * @param PayloadDTO $payload
     */
    public function setPayload(PayloadDTO $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * @return AudienceDTO
     */
    public function getAudience(): AudienceDTO
    {
        return $this->audience;
    }

    /**
     * @param AudienceDTO $audience
     */
    public function setAudience(AudienceDTO $audience): void
    {
        $this->audience = $audience;
    }
}
