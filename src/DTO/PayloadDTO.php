<?php

declare(strict_types=1);

namespace GravitecSDK\DTO;

/**
 * Class PayloadDTO.
 */
final class PayloadDTO
{
    /**
     * @var string
     */
    private $title = 'Title';

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $image = '';

    /**
     * @var string
     */
    private $redirectUrl;

    /**
     * @var ButtonDTO[]
     */
    private $buttons = [];

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     */
    public function setRedirectUrl(string $redirectUrl): void
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return ButtonDTO[]
     */
    public function getButtons(): array
    {
        return $this->buttons;
    }

    /**
     * @param ButtonDTO $button
     */
    public function addButton(ButtonDTO $button): void
    {
        $this->buttons[] = $button;
    }
}
