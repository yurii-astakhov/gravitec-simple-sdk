<?php

declare(strict_types=1);

namespace GravitecSDK\DTO;

/**
 * Class TagSegmentDTO.
 */
final class TagSegmentDTO
{
    /**
     * @var string
     */
    private $association;

    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getAssociation(): string
    {
        return $this->association;
    }

    /**
     * @param string $association
     */
    public function setAssociation(string $association): void
    {
        $this->association = $association;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
